<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Login</title>

        <!-- Bootstrap Core CSS -->
        <link href="../CSS/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../CSS/sb-admin-2.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><b>Dados do Usuário: </b></h3>
                            <?php
                            include('../BD/conexao.php');

                            $sql = "SELECT usuario_nome, usuario_email FROM usuarios WHERE usuario_login = '{$_POST['login']}'";
                            $stmt = $conn->prepare($sql);
                            $stmt->execute();
                            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

                            foreach ($result as $row) {
                                echo $row['usuario_nome'] . " | " . $row['usuario_email'] . " <br>";
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>