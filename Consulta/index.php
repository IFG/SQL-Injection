<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Consulta</title>

        <!-- Bootstrap Core CSS -->
        <link href="../CSS/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../CSS/sb-admin-2.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Consulta</h3>
                        </div>
                        <div class="panel-body">
                            <form method="post" action="inicio.php" role="form">
                                <fieldset>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="login" name="login" type="text" autofocus>
                                    </div>
                                    <button type="submit" name="botao" class="btn btn-success btn-block">Consultar</button>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>