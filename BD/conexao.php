<?php

ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);

$dsn = 'mysql:dbname=sql-injection;host=localhost';
$user = 'root';
$password = '';

try {
    $conn = new PDO($dsn, $user, $password);
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}