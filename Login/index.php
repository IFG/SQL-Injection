<?php
if (!isset($_SESSION)) {
    session_start();
}

//Login de Usários
if (isset($_POST['botao'])) {
    include('../BD/conexao.php');
    $erro = array();

    if (strlen($_POST['login']) <= 0) {
        $erro[] = "Preencha seu <strong>Login</strong> Corretamente.";
    }

    if (strlen($_POST['password']) <= 0) {
        $erro[] = "Preencha sua <strong>Senha</strong> Corretamente.";
    }

    if (count($erro) == 0) {
        $sql = "SELECT usuario_senha, usuario_nome FROM usuarios 
        WHERE usuario_login = '{$_POST['login']}' and usuario_senha = '{$_POST['password']}'";

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (!$result) {
            $erro[] = "<strong>E-mail</strong> ou <strong>Senha</strong> Incorreto.";
        } else {
            $_SESSION['login'] = $_POST['login'];
        }

        if (count($erro) == 0) {
            header('Location: inicio.php');
        }
    }
}
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Login</title>

        <!-- Bootstrap Core CSS -->
        <link href="../CSS/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="../CSS/sb-admin-2.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Login</h3>
                        </div>
                        <div class="panel-body">
                            <?php
                            if (isset($erro)) {
                                if (count($erro) > 0) {
                                    ?>
                                    <div class="alert alert-danger">
                                        <?php
                                        foreach ($erro as $msg)
                                            echo "$msg <br>";
                                        ?>
                                    </div>
                                    <?php
                                }
                            }
                            ?>

                            <form method="post" action="" role="form">
                                <fieldset>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="login" name="login" type="text" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Senha" name="password" type="password" value="">
                                    </div>
                                    <button type="submit" name="botao" class="btn btn-success btn-block">Login</button>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>